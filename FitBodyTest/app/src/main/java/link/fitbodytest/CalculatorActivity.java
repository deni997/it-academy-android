package link.fitbodytest;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;


public class CalculatorActivity extends MotherActivity implements OnFragmentSendMessageListener {

    ViewPager viewPager;
    CalculatorPagerAdapter calculatorPagerAdapter;

    TabLayout tabLayout;

    public static class CalculatorPagerAdapter extends FragmentPagerAdapter{

        static final int NUM_ITEMS = 3;
        static final String TAB_TITLES[] = new String[]{"Ideal weight","Calorie","Body Type"};

        public CalculatorPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    return new IdealWeightFragment();
                case 1:
                    return new CalorieFragment();
                case 2:
                    return new BodyTypeFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TAB_TITLES[position];
        }
    }



    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);



        viewPager = (ViewPager) findViewById(R.id.vPager);
        calculatorPagerAdapter = new CalculatorPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(calculatorPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                CharSequence title = calculatorPagerAdapter.getPageTitle(position);
                changeActionBarText(title.toString());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        int calculatorNum = this.getIntent().getIntExtra("CALCULATOR_NUM",0);

        if(viewPager.getCurrentItem() == calculatorNum){
            changeActionBarText(calculatorPagerAdapter.getPageTitle(calculatorNum).toString());
        }

        viewPager.setCurrentItem(calculatorNum);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onFragmentSendMessage(CharSequence message){
        showCalculatorToast(message);
    }
}

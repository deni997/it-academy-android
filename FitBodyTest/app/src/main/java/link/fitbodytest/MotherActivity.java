package link.fitbodytest;



import android.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Limitless on 28.6.2018.
 */

public class MotherActivity extends AppCompatActivity{
    Toast toast = null;

    public void showCalculatorToast(CharSequence message){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup)findViewById(R.id.custom_toast_layout));

        TextView text = (TextView) layout.findViewById(R.id.toast_message);
        text.setText(message);

        if(toast != null){
            toast.cancel();
        }

        toast = new Toast(this);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void showInfoToast(CharSequence message){

        
    }
    protected void changeActionBarText(String text){
        getSupportActionBar().setTitle(text);
    }

    protected void enableUpButton(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}

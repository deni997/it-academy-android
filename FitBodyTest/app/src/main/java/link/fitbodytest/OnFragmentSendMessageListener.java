package link.fitbodytest;

/**
 * Created by Limitless on 29.6.2018.
 */

public interface OnFragmentSendMessageListener {
    void onFragmentSendMessage(CharSequence message);
}

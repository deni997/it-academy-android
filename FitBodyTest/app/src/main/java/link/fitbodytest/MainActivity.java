package link.fitbodytest;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends MotherActivity implements OnFragmentSendMessageListener {

    View fragmentContainer;
    ListView drawerList;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    public void onFragmentSendMessage(CharSequence message){
        showCalculatorToast(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.drawer_open,R.string.drawer_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actionBarDrawerToggle.syncState();

        drawerList = (ListView)findViewById(R.id.left_drawer);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),CalculatorActivity.class);

                switch (position){
                    case 0:
                        intent.putExtra("CALCULATOR_NUM",0);
                        break;
                    case 1:
                        intent.putExtra("CALCULATOR_NUM",1);
                        break;
                    case 2:
                        intent.putExtra("CALCULATOR_NUM",2);
                        break;
                }

                startActivity(intent);
            }
        });

        fragmentContainer = findViewById(R.id.fragment_container);

        if(fragmentContainer != null) {

            if (savedInstanceState == null) {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                Fragment idealWeightFragment = new IdealWeightFragment();
                ft.addToBackStack("ideal_weight");
                ft.commit();
            }
        }

    }

    public void onIdealWeightClick(View v){

        if(fragmentContainer!=null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment idealWeightFragment = new IdealWeightFragment();
            ft.addToBackStack("ideal_weight");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.replace(fragmentContainer.getId(), idealWeightFragment);
            ft.commit();
        }else {
            Intent intent = new Intent(this,CalculatorActivity.class);
            intent.putExtra("CALCULATOR_NUM",0);
            startActivity(intent);
        }
    }

    public void onCalorieClick(View v){

        if(fragmentContainer!=null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment calorieFragment = new CalorieFragment();
            ft.addToBackStack("calorie");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.replace(fragmentContainer.getId(), calorieFragment);

            ft.commit();
        }else {
            Intent intent = new Intent(this,CalculatorActivity.class);
            intent.putExtra("CALCULATOR_NUM",1);
            startActivity(intent);
        }
    }

    public void onBodyTypeClick(View v){

        if(fragmentContainer!=null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment bodyTypeFragment = new BodyTypeFragment();
            ft.addToBackStack("body_type");
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.replace(fragmentContainer.getId(), bodyTypeFragment);

            ft.commit();
        }else {
            Intent intent = new Intent(this,CalculatorActivity.class);
            intent.putExtra("CALCULATOR_NUM",2);
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.action_bar_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch(item.getItemId()){
            case R.id.info_item:
                int spannableColor = ContextCompat.getColor(this,R.color.primary_text);
                String infoText = "FIT BODY\nDeni Dujmovic\nITAcademy\nLINK GROUP";

                Spannable spannable = new SpannableString(infoText);
                spannable.setSpan(new ForegroundColorSpan(spannableColor),0,infoText.indexOf("\n"),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannable.setSpan(new RelativeSizeSpan(0.7f),infoText.indexOf("\n"),infoText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                showInfoToast(spannable);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
}

package e.limitless.todo.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import e.limitless.todo.core.ToDoDatabaseContract.ItemTable;
import e.limitless.todo.core.ToDoDatabaseContract.PriorityTable;

public class ToDoDbHelper extends SQLiteOpenHelper {

    public ToDoDbHelper(Context context) {
        super(context,ToDoDatabaseContract.DATABASE_NAME,null,ToDoDatabaseContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ItemTable.SQL_CREATE_ITEMS_TABLE);
        db.execSQL(PriorityTable.SQL_CREATE_PRIORITY_TABLE);

        for (int i = 0; i < PriorityTable.SQL_CREATE_INITIAL_PRIORITY_DATA.length; i++) {
            db.execSQL(PriorityTable.SQL_CREATE_INITIAL_PRIORITY_DATA[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ItemTable.SQL_DELETE_ITEMS_TABLE);
        db.execSQL(PriorityTable.SQL_DELETE_PRIORITY_TABLE);
        onCreate(db);
    }


}
